@extends('layout')


@section('contenido')

<h1>Contactos</h1>

{{--<ul>
	@foreach($lista_contactos as $contacto)
		<li>{{ $contacto }}</li>
	@endforeach--}}

	{{--@forelse($lista_contactos as $contacto)
		<li>{{ $contacto }}</li>
	@empty
		<h5>No tiene contactos :(</h5>
	@endforelse

	@if(count($lista_contactos) === 0)
		<h6>No tiene contactos :( x2</h6>
	{{--@elseif(count($lista_contactos) > 0)--}}
	{{--@@else
		<h6>Tiene varios contactos :)</h6>
	@endif
</ul>--}}

<h2>Escríbeme</h2>
@if(session()->has('info'))
	<h3>{{ session('info') }}</h3>
@else
<form method="POST" action="contacto">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	{!! csrf_field() !!}
	<p><label for="nombre">
		Nombre
		<input type="text" name="nombre" value="{{ old('nombre') }}">
		{!! $errors->first('nombre', '<span class=error>:message</span>') !!}
	</label></p

	<p><label for="email">
		Email
		<input type="email" name="email" value="{{ old('email') }}">
		{!! $errors->first('email', '<span class=error>:message</span>') !!}
	</label></p>

	<p><label for="mensaje">
		Mensaje
		<textarea name="mensaje">{{ old('mensaje') }}</textarea>
		{!! $errors->first('mensaje', '<span class=error>:message</span>') !!}
	</label></p>

	<input type="submit" value="Enviar">
</form>
@endif

@stop
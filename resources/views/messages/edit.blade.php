@extends('layout')

@section('contenido')

<h1>Editar mensaje</h1>

<form method="POST" action="{{ route('mensajes.update', $message->id) }}">
	{!! method_field('PUT') !!}
	
	@include('messages.form', [
		'showFields' => !$message->user_id,
		'showEditFields' => true,
		'btnText' => 'Actualizar'
	])

</form>

@stop
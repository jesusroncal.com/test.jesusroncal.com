<input type="hidden" name="_token" value="{{ csrf_token() }}">
{!! csrf_field() !!}
<input type="hidden" name="user_id" value="{{ (auth()->check()) ? auth()->user()->id : null }}">

@if($showFields)
	<p><label for="nombre">
		Nombre
		<input class="form-control" type="text" name="nombre" value="{{ isset($message->nombre) ? $message->nombre : old('nombre') }}">
		{!! $errors->first('nombre', '<div class="alert alert-warning" role="alert">:message</div>') !!}
	</label></p>

	<p><label for="email">
		Email
		<input class="form-control" type="email" name="email" value="{{ isset($message->email) ? $message->email : old('email') }}">
		{!! $errors->first('email', '<div class="alert alert-warning" role="alert">:message</div>') !!}
	</label></p>
@endif

<p><label for="mensaje">
	Mensaje
	<textarea class="form-control" name="mensaje">{{ isset($message->mensaje) ? $message->mensaje : old('mensaje') }}</textarea>
	{!! $errors->first('mensaje', '<div class="alert alert-warning" role="alert">:message</div>') !!}
</label></p>

@if($showEditFields)
	<p><label for="note">
		Nota
		<input class="form-control" type="text" name="note" value="{{ isset($message->note->body) ? $message->note->body : '' }}">
	</label></p>

	<div class="checkbox">
		@foreach($tags as $id => $name)
			<label for="tags">
				<input
					type="checkbox"
					value="{{ $id }}"
					{{ $message->tags->pluck('id')->contains($id) ? 'checked' : ''}}
					name="tags[]">
				{{ $name }}
			</label>
		@endforeach
	</div>
	{!! $errors->first('tags', '<div class="alert alert-danger" role="alert">:message</div>') !!}
@endif

<input class="btn btn-primary" type="submit" value="{{ $btnText }}">
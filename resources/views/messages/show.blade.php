@extends('layout')

@section('contenido')

<h1>Mensaje</h1>

@if($message->user_id)
	<p>Enviado por {{ $message->user->name }} - {{ $message->user->email }}</p>
@else
	<p>Enviado por {{ $message->nombre }} - {{ $message->email }}</p>
@endif

<p>{{ $message->mensaje }}</p>

@stop
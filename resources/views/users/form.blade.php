<input type="hidden" name="_token" value="{{ csrf_token() }}">
{!! csrf_field() !!}


<p><label for="name">
	Name
	<input class="form-control" type="text" name="name" value="{{ isset($user->name) ? $user->name : old('name') }}">
	{!! $errors->first('name', '<div class="alert alert-danger" role="alert">:message</div>') !!}
</label></p>

<p><label for="email">
	Email
	<input class="form-control" type="email" name="email" value="{{ isset($user->email) ? $user->email : old('email') }}">
	{!! $errors->first('email', '<div class="alert alert-danger" role="alert">:message</div>') !!}
</label></p>


@unless($user->id)
	<p><label for="password">
		Password
		<input class="form-control" type="password" name="password">
		{!! $errors->first('password', '<div class="alert alert-danger" role="alert">:message</div>') !!}
	</label></p>

	<p><label for="password_confirmation">
		Password Confirm
		<input class="form-control" type="password" name="password_confirmation">
		{!! $errors->first('password_confirmation', '<div class="alert alert-danger" role="alert">:message</div>') !!}
	</label></p>
@endunless


<div class="checkbox">
	@foreach($roles as $id => $display_name)
		<label for="roles">
			<input
				type="checkbox"
				value="{{ $id }}"
				{{ $user->roles->pluck('id')->contains($id) ? 'checked' : ''}}
				name="roles[]">
			{{ $display_name }}
		</label>
	@endforeach
</div>
{!! $errors->first('roles', '<div class="alert alert-danger" role="alert">:message</div>') !!}

<p><label for="note">
	Nota
	<input class="form-control" type="text" name="note" value="{{ isset($user->note->body) ? $user->note->body : old('note') }}">
</label></p>

<div class="checkbox">
	@foreach($tags as $id => $name)
		<label for="tags">
			<input
				type="checkbox"
				value="{{ $id }}"
				{{ $user->tags->pluck('id')->contains($id) ? 'checked' : ''}}
				name="tags[]">
			{{ $name }}
		</label>
	@endforeach
</div>
{!! $errors->first('tags', '<div class="alert alert-danger" role="alert">:message</div>') !!}
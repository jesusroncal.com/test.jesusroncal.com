@extends('layout')

@section('contenido')

<h1>Todos los usuarios</h1>

<a href="{{ route('usuarios.create') }}" class="btn btn-primary float-right">Crear nuevo usuario</a>

<table width="100%" class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Email</th>
			<th>Roles</th>
			<th>Notas</th>
			<th>Etiquetas</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>
					<a href="{{ route('usuarios.show', $user->id) }}">{{ $user->name }}</a>
				</td>
				<td>{{ $user->email }}</td>
				<td>
					{{ $user->roles->pluck('display_name')->implode(' - ') }}
					{{--@foreach($user->roles as $role)
						<span class="badge badge-pill badge-dark">
						{{ $role->display_name }}
					</span>
					@endforeach--}}
				</td>
				<td>{{ isset($user->note->body) ? $user->note->body : '' }}</td>
				<td>{{ isset($user->tags) ? $user->tags->pluck('name')->implode(', ') : ''}}</td>
				<td>
					<a class="btn btn-primary btn-sm" href="{{ route('usuarios.edit', $user->id) }}">Editar</a>
					<form style="display: inline;" method="POST" action="{{ route('usuarios.destroy', $user->id) }}">
						{!! method_field('DELETE') !!}
						{!! csrf_field() !!}

						<button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
					</form>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@stop
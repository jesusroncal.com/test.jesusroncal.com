@extends('layout')

@section('contenido')

<h1>{{ $user->name }}</h1>

<table class="table">
	<tr>
		<th>Nombre: </th>
		<th>{{ $user->name }}</th>
	</tr>
	<tr>
		<th>Email: </th>
		<th>{{ $user->email }}</th>
	</tr>
	<tr>
		<th>Roles: </th>
		<td>
			<ul>
				@foreach($user->roles as $role)
					<li>{{ $role->display_name }}</li>
				@endforeach
			</ul>
		</td>
	</tr>
</table>

@can('edit', $user)
	<a href="{{ route('usuarios.edit', $user->id) }}" class="btn btn-info">Editar</a>
@endcan

@can('destroy', $user)
	<form style="display: inline;" method="POST" action="{{ route('usuarios.destroy', $user->id) }}">
		{!! method_field('DELETE') !!}
		{!! csrf_field() !!}

		<button class="btn btn-danger" type="submit">Eliminar</button>
	</form>
@endcan

@stop
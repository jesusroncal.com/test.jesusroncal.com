<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/css/app.css">
	<title>Mi sitio</title>
</head>
<body>
	<header>
		<?php
			function activeMenu($url)
			{
				return request()->is($url) ? 'active' : '';
			}
		?>
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			  <a class="navbar-brand" href="#">Menú</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			
			  <div class="collapse navbar-collapse" id="navbarNav">
			    <ul class="navbar-nav mr-auto">
			      <li class="nav-item {{ activeMenu('/') }}">
			        <a class="nav-link" href="{{ route('home') }}">Inicio <span class="sr-only">(current)</span></a>
			      </li>
			      <li class="nav-item {{ activeMenu('saludos*') }}">
					<a class="nav-link" href="{{ route('saludos', 'Jesus') }}">Saludo</a>
			      </li>
			      <li class="nav-item {{ activeMenu('mensajes/*') }}">
					<a class="nav-link" href="{{ route('mensajes.create') }}">Contacto</a>
			      </li>
			      @if(auth()->check())
				      <li class="nav-item {{ activeMenu('mensajes') }}">
						<a class="nav-link" href="{{ route('mensajes.index') }}">Mensajes</a>
				      </li>
				      
				      @if(auth()->user()->hasRoles(['admin', 'moderador']))
					      <li class="nav-item {{ activeMenu('usuarios') }}">
							<a class="nav-link" href="{{ route('usuarios.index') }}">Usuarios</a>
					      </li>
					  @endif
			      @endif
			    </ul>
			    <ul class="navbar-nav navbar-toggler-right">
					@if(auth()->guest())
			    		<li class="nav-item {{ activeMenu('login') }}">
			    			<a class="nav-link" href="/login">Login</a>
			    		</li>
					@else
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{ auth()->user()->name }}
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="/usuarios/{{ auth()->id() }}/edit">Mi cuenta</a>
								<a class="dropdown-item" href="/logout">Logout</a>
							</div>
						</li>
			    	@endif
			    </ul>
			  </div>
			</nav>
		</div>
	</header>
	
	<br>

	<div class="container">
		@if(in_array(Route::currentRouteName(), ['mensajes.create', 'mensajes.show', 'mensajes.edit']))
			<a class="btn btn-info" href="{{ route('mensajes.index') }}">
				<<
			</a>
		@endif

		@if(in_array(Route::currentRouteName(), ['usuarios.create', 'usuarios.show', 'usuarios.edit']))
			<a class="btn btn-info" href="{{ route('usuarios.index') }}">
				<<
			</a>
		@endif

		@yield('contenido')

		<footer>
			<b>Copyright ® {{ date('Y') }}</b>
		</footer>
	</div>

	<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
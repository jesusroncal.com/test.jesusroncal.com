<?php

namespace App\Http\Controllers;

use App\Message;
use App\Note;
use App\Tag;
use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\CreateMessageRequest;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$messages = DB::table('messages')->get();
        $messages = Message::with(['user', 'note', 'tags'])->get();

        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMessageRequest $request)
    {
        /*DB::table('messages')->insert([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);*/

        /*$message = new Message;
        $message->nombre = $request->input('nombre');
        $message->email = $request->input('email');
        $message->mensaje = $request->input('mensaje');
        $message->save();*/

        //dd($request->all());
        //Model::unguard();

        $message = Message::create($request->all());

        Mail::send('emails.contact', ['msg' => $message], function($m) use($message)
        {
            $m->to($message->email, $message->nombre)->subject('Tu mensaje fue recibido');
        });

        return redirect()->route('mensajes.create')->with('info', 'Hemos recibido tu mensaje.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$message = DB::table('messages')->where('id', $id)->first();
        $message = Message::findOrFail($id);

        return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$message = DB::table('messages')->where('id', $id)->first();
        $message = Message::findOrFail($id);
        $tags = Tag::pluck('name', 'id');

        return view('messages.edit', compact('message', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function returnFalse()
    {
        return 0;
    }

    public function update(CreateMessageRequest $request, $id)
    {
        try {
            DB::beginTransaction();
                $message = Message::with('note')->findOrFail($id);

                (
                    Note::whereHasMorph('notable', [Message::class], function($query) use($message) {
                        $query->where('notable_id', isset($message->note->notable_id) ? $message->note->notable_id : 0);
                    })->count()
                )
                ?
                    $message->note()->update(['body' => $request->note])

                :
                    $message->note()->create(['body' => $request->note]);

                $message->update($request->only('nombre', 'email', 'mensaje'));
                $message->tags()->sync($request->tags);
                /*($message->note()->update(['body' => $request->note]) === 0)
                    ? $message->note()->create(['body' => $request->note]) : '';*/
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('mensajes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DB::table('messages')->where('id', $id)->delete();
        $message = Message::findOrFail($id);
        $message->note()->delete();
        $message->delete();

        return redirect()->route('mensajes.index');    }
    }

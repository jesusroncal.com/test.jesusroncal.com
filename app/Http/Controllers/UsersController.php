<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Note;
use App\Tag;
use App\Taggable;
use DB;
use App\Http\Requests\CreateUserRequest;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('roles:admin', ['except' => ['edit', 'update', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['roles' ,'note', 'tags'])->get();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id');

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateUserRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->attach($request->roles);

        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('edit', $user);
        $roles = Role::pluck('display_name', 'id');
        $tags = Tag::pluck('name', 'id');

        //dd($user->id);

        return view('users.edit', compact('user', 'roles', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            DB::beginTransaction();
                $user = User::with('note', 'tags')->findOrFail($id);
                $this->authorize('update', $user);
                
                (
                    Note::whereHasMorph('notable', [User::class], function($query) use($user)
                    {
                        $query->where('notable_id', isset($user->note->notable_id) ? $user->note->notable_id : 0);
                    })->count()
                )
                ?
                    $user->note()->update(['body' => $request->note])
                :
                    $user->note()->create(['body' => $request->note]);

                $user->update($request->only('name', 'email'));
                $user->roles()->sync($request->roles);
                $user->tags()->sync($request->tags);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return back()->with('info', 'Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('destroy', $user);
        $user->delete();

        return back();
    }
}

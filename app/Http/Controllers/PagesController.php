<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateMessageRequest;

class PagesController extends Controller
{
    /*protected $request;

    public function __construct(Request $request)
    {
    	$this->request = $request;
    }*/

    /*public function __construct()
    {
    	$this->middleware('example', ['except' => ['home']]);
    }*/

    public function home()
    {
    	return view('home');
    }

    /*public function contact()
    {
    	$lista_contactos = [
	    	'Jose', 'Kaisy', 'Miguel'
	    ];

	    return  view('contactos', compact('lista_contactos'));
    }*/

    /*public function mensajes(CreateMessageRequest $request)
    {*/
    	/*if($request->filled('nombre'))
    	{
    		return 'Tiene nombre. Es ' . $request->input('nombre');
    	}

    	return 'No tiene nombre';*/

    	//$data = $request->all();

    	/*return response('Contenido de la respuesta', 201)
    			->header('X-TOKEN', 'secret')
    			->header('X-TOKEN-2', 'secret-2')
    			->cookie('X-Cookie', 'cookie');

    	return response()->json([
    				'data' => $data
    			], 202)
    			->header('X-TOKEN', 'secret');

    	return redirect()
    			->route('contactos')
    			->with('info', 'Tu mensaje ha sido enviado exitosamente :)');*/

    	/*return back()
    			->with('info', 'Tu mensaje ha sido enviado exitosamente :)');
    }*/

    public function saludo($nombre = "Invitado")
    {
    	//return view('saludo', ['nombre' => $nombre]);
		//return view('saludo')->with(['nombre' => $nombre]);
		return view('saludo', compact('nombre'));
    }
}
